"""Grid processing tools"""
import numpy as np
import matplotlib.pyplot as plt
import geopandas as gp
import mapclassify as mc
from typing import Tuple, List, Optional, Dict
from pathlib import Path
import pandas as pd
from pyproj import Transformer
from matplotlib import colors
from IPython.display import clear_output, display
from . import tools
import warnings

CHUNK_SIZE = 5000000

# reference metric names and column names
COLUMN_METRIC_REF = {
    "postcount_est": "post_hll",
    "usercount_est": "user_hll",
    "userdays_est": "date_hll"}

DTYPES = {'latitude': float, 'longitude': float}

OUTPUT = Path.cwd().parents[0] / "out"

def reset_metrics(
        grid: gp.GeoDataFrame,
        metrics: List[str] = [
            "postcount_est", "usercount_est", "userdays_est",
            "postcount_hll", "usercount_hll", "userdays_hll"],
        setzero: bool = None):
    """Remove columns from GeoDataFrame and optionally fill with 0"""
    for metric in metrics:
        try:
            grid.drop(metric, axis=1, inplace=True)
            grid.drop(f'{metric}_cat', axis=1, inplace=True)
        except KeyError:
            pass
        if setzero:
            grid.loc[:, metric] = 0


def get_best_bins(
        search_values_x: np.array, search_values_y: np.array,
        xbins: np.array, ybins: np.array) -> Tuple[np.ndarray, np.ndarray]:
    """Will return best bin for a lat and lng input
    
    Note: prepare bins and values in correct matching projection
    
    Args:
        search_values_y: A list of projected latitude values
        search_values_x: A list of projected longitude values
        xbins: 1-d array of bins to snap lat/lng values
        ybins: 1-d array of bins to snap lat/lng values

    Returns:
        Tuple[int, int]: A list of tuples with 2 index positions for the best 
            matching bins for each lat/lng
    """
    xbins_idx = np.digitize(search_values_x, xbins, right=False)
    ybins_idx = np.digitize(search_values_y, ybins, right=False)
    return (xbins[xbins_idx-1], ybins[ybins_idx-1])


def bin_coordinates(
        df: pd.DataFrame, xbins:
        np.ndarray, ybins: np.ndarray) -> pd.DataFrame:
    """Bin coordinates using binary search and append to df as new index"""
    xbins_match, ybins_match = get_best_bins(
        search_values_x=df['x'].to_numpy(),
        search_values_y=df['y'].to_numpy(),
        xbins=xbins, ybins=ybins)
    # append target bins to original dataframe
    # use .loc to avoid chained indexing
    df.loc[:, 'xbins_match'] = xbins_match
    df.loc[:, 'ybins_match'] = ybins_match
    # drop x and y columns not needed anymore
    df.drop(columns=['x', 'y'], inplace=True)


def bin_chunked_coordinates(
        chunked_df: List[pd.DataFrame], ybins: np.array, xbins: np.array):
    """Bin coordinates of chunked dataframe"""
    binned_cnt = 0
    for ix, df in enumerate(chunked_df):
        bin_coordinates(df, xbins, ybins)
        df.set_index(['xbins_match', 'ybins_match'], inplace=True)
        clear_output(wait=True)
        binned_cnt += len(df)
        print(f"Binned {binned_cnt:,.0f} coordinates..")


def union_hll(
        hll_series: pd.Series,
        db_calc: tools.DbConn,
        cardinality: bool = True,
        ) -> pd.Series:
    """HLL Union and (optional) cardinality estimation from series of hll sets
    based on group by composite index.

        Args:
        hll_series: Indexed series (bins) of hll sets. 
        cardinality: If True, returns cardinality (counts). Otherwise,
            the unioned hll set will be returned.
            
    The method will combine all groups of hll sets first,
        in a single SQL command. Union of hll hll-sets belonging 
        to the same group (bin) and (optionally) returning the cardinality 
        (the estimated count) per group will be done in postgres.
    
    By utilizing Postgres´ GROUP BY (instead of, e.g. doing 
        the group with numpy), it is possible to reduce the number
        of SQL calls to a single run, which saves overhead 
        (establishing the db connection, initializing the SQL query 
        etc.). Also note that ascending integers are used for groups,
        instead of their full original bin-ids, which also reduces
        transfer time.
    
    cardinality = True should be used when calculating counts in
        a single pass.
        
    cardinality = False should be used when incrementally union
        of hll sets is required, e.g. due to size of input data.
        In the last run, set to cardinality = True.
    """
    # group all hll-sets per index (bin-id)
    series_grouped = hll_series.groupby(
        hll_series.index).apply(list)
    # From grouped hll-sets,
    # construct a single SQL Value list;
    # if the following nested list comprehension
    # doesn't make sense to you, have a look at
    # spapas.github.io/2016/04/27/python-nested-list-comprehensions/
    # with a decription on how to 'unnest'
    # nested list comprehensions to regular for-loops
    hll_values_list = ",".join(
        [f"({ix}::int,'{hll_item}'::hll)" 
         for ix, hll_items
         in enumerate(series_grouped.values.tolist())
         for hll_item in hll_items])
    # Compilation of SQL query,
    # depending on whether to return the cardinality
    # of unioned hll or the unioned hll
    return_col = "hll_union"
    hll_calc_pre = ""
    hll_calc_tail = "AS hll_union"
    if cardinality:
        # add sql syntax for cardinality 
        # estimation
        # (get count distinct from hll)
        return_col = "hll_cardinality"
        hll_calc_pre = "hll_cardinality("
        hll_calc_tail = ")::int"
    db_query = f"""
        SELECT sq.{return_col} FROM (
            SELECT s.group_ix,
                   {hll_calc_pre}
                   hll_union_agg(s.hll_set)
                   {hll_calc_tail}
            FROM (
                VALUES {hll_values_list}
                ) s(group_ix, hll_set)
            GROUP BY group_ix
            ORDER BY group_ix ASC) sq
        """
    df = db_calc.query(db_query)
    # to merge values back to grouped dataframe,
    # first reset index to ascending integers
    # matching those of the returned df;
    # this will turn series_grouped into a DataFrame;
    # the previous index will still exist in column 'index'
    df_grouped = series_grouped.reset_index()
    # drop hll sets not needed anymore
    df_grouped.drop(columns=[hll_series.name], inplace=True)
    # append hll_cardinality counts 
    # using matching ascending integer indexes
    df_grouped.loc[df.index, return_col] = df[return_col]
    # set index back to original bin-ids
    df_grouped.set_index("index", inplace=True)
    # split tuple index to produce
    # the multiindex of the original dataframe
    # with xbin and ybin column names
    df_grouped.index = pd.MultiIndex.from_tuples(
        df_grouped.index, names=['xbin', 'ybin'])
    # return column as indexed pd.Series
    return df_grouped[return_col]


def join_df_grid(
        df: pd.DataFrame, grid: gp.GeoDataFrame,
        ybins: np.array, xbins: np.array,
        metric: str = "postcount_est",
        cardinality: bool = True,
        column_metric_ref: Dict[str, str] = COLUMN_METRIC_REF,
        ):
    """Union HLL Sets and estimate postcount per 
    grid bin from lat/lng coordinates
    
        Args:
        df: A pandas dataframe with latitude and 
            longitude columns in WGS1984
        grid: A geopandas geodataframe with indexes 
            x and y (projected coordinates) and grid polys
        metric: target column for estimate aggregate.
            Default: postcount_est.
        cardinality: will compute cardinality of unioned
            hll sets. Otherwise, unioned hll sets will be 
            returned for incremental updates.
    """
    # optionally, bin assigment of projected coordinates,
    # make sure to not bin twice:
    # x/y columns are removed after binning
    if 'x' in df.columns:
        bin_coordinates(df, xbins, ybins)
        # set index column
        df.set_index(
            ['xbins_match', 'ybins_match'], inplace=True)
    # union hll sets and 
    # optional estimate count distincts (cardinality)
    column = column_metric_ref.get(metric)
    # get series with grouped hll sets
    hll_series = df[column]
    # union of hll sets:
    # to allow incremental union of already merged data
    # and new data, concatenate series from grid and new df
    # only if column with previous hll sets already exists
    if metric in grid.columns:
        # remove nan values from grid and
        # rename series to match names
        hll_series = pd.concat(
            [hll_series, grid[metric].dropna()]
            ).rename(column)
    cardinality_series = union_hll(
        hll_series, cardinality=cardinality)
    # add unioned hll sets/computed cardinality to grid
    grid.loc[
        cardinality_series.index, metric] = cardinality_series
    if cardinality:
        # set all remaining grid cells
        # with no data to zero and
        # downcast column type from float to int
        grid[metric] = grid[metric].fillna(0).astype(int)


def join_chunkeddf_grid(
        chunked_df: List[pd.DataFrame], grid: gp.GeoDataFrame,
        db_calc: tools.DbConn,
        metric: str = "postcount_est", chunk_size: int = CHUNK_SIZE,
        keep_hll: Optional[bool] = None,
        column_metric_ref: Dict[str, str] = COLUMN_METRIC_REF,
        ):
    """Incremental union of HLL Sets and estimate postcount per 
    grid bin from chunked list of dataframe records. Results will
    be stored in grid.
    
    Args:
    chunked_df: A list of (chunked) dataframes with latitude and 
        longitude columns in WGS1984
    grid: A geopandas geodataframe with indexes 
        x and y (projected coordinates) and grid polys
    metric: target column for estimate aggregate.
        Default: postcount_est.
    keep_hll: If True, will not remove HLL sketches after
        final cardinality estimation. Will not reset metrics.
    """
    reset_metrics(grid, [metric])
    if keep_hll:
        # if existing hll sets present,
        # rename column so it can be updates
        metric_hll = metric.replace("_est", "_hll")
        if metric_hll in grid.columns:
            grid.rename(
                columns={
                    metric_hll: metric.replace(
                        "_hll", "_est")},
                inplace=True)
    for ix, chunk_df in enumerate(chunked_df):
        # compute cardinality only on last iteration
        cardinality = False
        if ix == len(chunked_df)-1:
            cardinality = True
        column = column_metric_ref.get(metric)
        # get series with grouped hll sets
        hll_series = chunk_df[column]
        if metric in grid.columns:
            # merge existing hll sets with new ones
            # into one series (with duplicate indexes);
            # remove nan values from grid and
            # rename series to match names
            hll_series = pd.concat(
                [hll_series, grid[metric].dropna()]
                ).rename(column)
        cardinality_series = union_hll(
            hll_series, cardinality=cardinality, db_calc=db_calc)
        if keep_hll:
            # only if final hll sketches need to
            # be kept (e.g. for benchmarking):
            # do another union, without cardinality
            # estimation, and store results
            # in column "metric"_hll
            hll_sketch_series = union_hll(
                hll_series, cardinality=False, db_calc=db_calc)
            grid.loc[
                hll_sketch_series.index,
                f'{metric.replace("_est", "_hll")}'] = hll_sketch_series
        # add unioned hll sets/computed cardinality to grid
        grid.loc[
            cardinality_series.index, metric] = cardinality_series
        if cardinality:
            # set all remaining grid cells
            # with no data to zero and
            # downcast column type from float to int
            grid[metric] = grid[metric].fillna(0).astype(int)
        clear_output(wait=True)
        print(f'Mapped ~{(ix+1)*chunk_size} coordinates to bins')


def proj_report(df, cnt, proj_transformer: Transformer, inplace: bool = False):
    """Project df with progress report"""
    proj_df(df, proj_transformer=proj_transformer)
    clear_output(wait=True)
    print(f'Projected {cnt:,.0f} coordinates')
    if inplace:
        return
    return df


def read_project_chunked(
        filename: str,
        usecols: List[str], chunk_size: int = CHUNK_SIZE,
        bbox: Tuple[float, float, float, float] = None, 
        dtypes: Dict = DTYPES,
        proj_transformer: Transformer = None) -> List[pd.DataFrame]:
    """Read data from csv, optionally clip to bbox and projet"""
    iter_csv = pd.read_csv(
        filename, usecols=usecols, iterator=True,
        dtype=dtypes, encoding='utf-8', chunksize=chunk_size)
    if bbox:
        chunked_df = [filter_df_bbox(
            df=chunk_df, bbox=bbox, inplace=False)
            for chunk_df in iter_csv]
    else:
        chunked_df = [chunk_df for chunk_df in iter_csv]
    # project
    projected_cnt = 0
    for chunk_df in chunked_df:
        projected_cnt += len(chunk_df)
        proj_report(
            chunk_df, projected_cnt, inplace=True,
            proj_transformer=proj_transformer)
    return chunked_df


def filter_df_bbox(
        df: pd.DataFrame, bbox: Tuple[float, float, float, float],
        inplace: bool = True):
    """Filter dataframe with bbox on latitude and longitude column"""
    df.query(
        f'({bbox[0]} < longitude) & '
        f'(longitude <  {bbox[2]}) & '
        f'({bbox[1]} < latitude) & '
        f'(latitude < {bbox[3]})',
        inplace=True)
    # set index to asc integers
    if inplace:
        df.reset_index(inplace=True, drop=True)
        return
    return df.reset_index(inplace=False, drop=True)


def proj_df(df, proj_transformer: Transformer):
    """Project pandas dataframe latitude and longitude decimal degrees
    using predefined proj_transformer
    """
    if 'longitude' not in df.columns:
        return
    xx, yy = proj_transformer.transform(
        df['longitude'].values, df['latitude'].values)
    # assign projected coordinates to
    # new columns x and y
    # the ':' means: replace all values in-place
    df.loc[:, "x"] = xx
    df.loc[:, "y"] = yy
    # Drop WGS coordinates
    df.drop(columns=['longitude', 'latitude'], inplace=True)


def format_legend(
        leg, bounds: List[str], inverse: bool = None,
        metric: str = "postcount_est"):
    """Formats legend (numbers rounded, colors etc.)"""
    leg.set_bbox_to_anchor((-0.2, 0.2, 0.2, 0.3))
    # get all the legend labels
    legend_labels = leg.get_texts()
    plt.setp(legend_labels, fontsize='6')
    lcolor = 'black'
    if inverse:
        frame = leg.get_frame()
        frame.set_facecolor('black')
        frame.set_edgecolor('grey')
        lcolor = "white"
    plt.setp(legend_labels, color=lcolor)
    if metric == "postcount_est":
        leg.set_title("Estimated Post Count")
    elif metric == "usercount_est":
        leg.set_title("Estimated User Count")
    else:
        leg.set_title("Estimated User Days")
    plt.setp(leg.get_title(), fontsize='10')
    leg.get_title().set_color(lcolor)
    # replace the numerical legend labels
    for bound, legend_label in zip(bounds, legend_labels):
        legend_label.set_text(bound)


def _rnd_f(f: float, dec: int = None) -> str:
    if dec is None:
        # return f'{f}'
        dec = 0
    return f'{f:,.{dec}f}'


def format_bound(
        upper_bound: float = None, lower_bound: float = None, 
        decimals: Optional[int] = None) -> str:
    """Format legend text for class bounds"""
    if upper_bound is None:
        return _rnd_f(lower_bound, decimals)
    if lower_bound is None:
        return _rnd_f(upper_bound, decimals)
    return f'{_rnd_f(lower_bound, decimals)} - {_rnd_f(upper_bound, decimals)}'


def min_decimals(num1: float, num2: float) -> int:
    """Return number of minimum required decimals"""
    if _rnd_f(num1) != _rnd_f(num2):
        return 0
    for i in range(1, 5):
        if _rnd_f(num1, i) != _rnd_f(num2, i):
            return i
    return 5


def get_label_bounds(
        scheme_classes: np.ndarray, metric_series: pd.Series,
        flat: bool = None) -> List[str]:
    """Get all upper bounds in the scheme_classes category"""
    upper_bounds = scheme_classes
    # get and format all bounds
    bounds = []
    for idx, upper_bound in enumerate(upper_bounds):
        if idx == 0:
            lower_bound = metric_series.min()
            decimals = None
        else:
            decimals = min_decimals(
                upper_bounds[idx-1], upper_bounds[idx])
            lower_bound = upper_bounds[idx-1]
        if flat:
            bound = format_bound(
                lower_bound=lower_bound,
                decimals=decimals)
        else:
            bound = format_bound(
                upper_bound, lower_bound,
                decimals=decimals)
        bounds.append(bound)
    if flat:
        upper_bound = format_bound(
            upper_bound=upper_bounds[-1],
            decimals=decimals)
        bounds.append(upper_bound)
    return bounds


def get_scheme_breaks(series_nan: pd.Series, scheme: str):
    """Classify series of values
    
    Notes: some classification schemes (e.g. HeadTailBreaks)
        do not support specifying the number of classes returned
        construct optional kwargs with k == number of classes
    """
    optional_kwargs = {"k":9}
    if scheme == "HeadTailBreaks":
        optional_kwargs = {}
    scheme_breaks = mc.classify(
        y=np.abs(series_nan.values), scheme=scheme, **optional_kwargs)
    return scheme_breaks


def classify_data(
        values_series: pd.Series,
        scheme: str):
    """Classify data (value series) and return classes,
       bounds, and colormap
       
    Args:
        values_series: A pandas.Series with metric column to classify
        scheme: The classification scheme to use.
        
    Adapted from:
        https://stackoverflow.com/a/58160985/4556479
    See available colormaps:
        http://holoviews.org/user_guide/Colormaps.html
    See available classification schemes:
        https://pysal.org/mapclassify/api.html
    """
    scheme_breaks = get_scheme_breaks(values_series, scheme)
    # get label bounds as flat array
    bounds = get_label_bounds(
        scheme_breaks.bins, values_series.dropna().values)
    return bounds, scheme_breaks


def label_nodata(
        grid: gp.GeoDataFrame, inverse: bool = None,
        metric: str = "postcount_est", scheme: str = None,
        cmap_name: str = None):
    """Add white to a colormap to represent missing value
    
    Adapted from:
        https://stackoverflow.com/a/58160985/4556479
        
    See available colormaps:
        http://holoviews.org/user_guide/Colormaps.html
    """
    if cmap_name is None:
        cmap_name = "OrRd"
    if scheme is None:
        scheme = "HeadTailBreaks"
    # set 0 to NaN
    grid_nan = grid[metric].replace(0, np.nan)
    # classify values and get scheme breaks
    bounds, scheme_breaks = classify_data(
        values_series=grid_nan.dropna(), 
        scheme=scheme)
    grid[f'{metric}_cat'] = scheme_breaks.find_bin(
        grid_nan).astype('str')
    # set label for NaN values
    grid.loc[grid_nan.isnull(), f'{metric}_cat'] = 'No Data'
    nodata_color = 'white'
    if inverse:
        nodata_color = 'black'
        cmap_name = 'cet_fire'
    cmap = plt.cm.get_cmap(cmap_name, scheme_breaks.k)
    # get hex values
    cmap_list = [colors.rgb2hex(cmap(i)) for i in range(cmap.N)]
    # lighten or darken up first/last color a bit 
    # to offset from black or white background
    if cmap_name == "OrRd":
        if inverse:
            firstcolor = '#3E0100'
            cmap_list[0] = firstcolor
        else:
            lastcolor = '#440402'
            cmap_list.append(lastcolor)
            cmap_list.pop(0)
    if cmap_name == "bone":
        # modify last two colors
        cmap_list.pop(len(cmap_list)-1)
        lastcolor = '#C5D4D4'
        cmap_list.append(lastcolor)
        cmap_list.pop(len(cmap_list)-1)
        lastcolor = '#D1F5F2'
        cmap_list.append(lastcolor)
    # append nodata color
    cmap_list.append(nodata_color)
    cmap_with_nodata = colors.ListedColormap(cmap_list)
    return cmap_with_nodata, bounds


def plot_figure(
        grid: gp.GeoDataFrame, title: str, bg: gp.GeoDataFrame,
        inverse: bool = None,
        metric: str = "postcount_est", store_fig: str = None,
        output: Path = None, figsize: Tuple[int, int] = None,
        scheme: str = None, cmap_name: str = None, edgecolor: str = None):
    """Combine layers and plot"""
    # for plotting, there're some minor changes applied
    # to the dataframe (replace NaN values),
    # make a shallow copy here to prevent changes
    # to modify the original grid
    if figsize is None:
        figsize = (22, 28)
    if output is None:
        raise ValueError("output (Path) must be defined")
    grid_plot = grid.copy()
    # create new plot figure object with one axis
    fig, ax = plt.subplots(1, 1, figsize=(figsize))
    ax.set_title(title, fontsize=10)
    print("Classifying bins..")
    cmap_with_nodata, bounds = label_nodata(
        grid=grid_plot, inverse=inverse, metric=metric, scheme=scheme,
        cmap_name=cmap_name)
    base = grid_plot.plot(
        ax=ax,
        column=f'{metric}_cat', cmap=cmap_with_nodata, legend=True)
    print("Formatting legend..")
    leg = ax.get_legend()
    format_legend(leg, bounds, inverse, metric)
    # combine with world geometry
    if edgecolor is None:
        edgecolor = 'black'
    ax.set_facecolor('white')
    ax.axis('off')
    if inverse:
        edgecolor = 'white'
        ax.set_facecolor('black')
    plot = bg.plot(
        ax=base, color='none', edgecolor=edgecolor, linewidth=0.1)
    if store_fig:
        print("Storing figure as png..")
        filename = f'{store_fig}.png'
        if inverse:
            filename = f'{store_fig}_inverse.png'
        fig.savefig(
            output / "figures" / filename, dpi=300, format='PNG',
            bbox_inches='tight', pad_inches=1)
    return plot


def filter_nullisland_df(
        df: Optional[pd.DataFrame] = None, 
        df_list: Optional[List[pd.DataFrame]] = None,
        col_x: str = "longitude", col_y: str = "latitude"):
    """Remove records from df inplace where both x and y coordinate are 0"""
    if df is not None:
        df_list = [df]
    if not df_list:
        raise ValueError("Please provide either df or df_list")
    for df in df_list:
        if col_x in df.columns:
            df.query(
                f'({col_x} == 0 and {col_y} == 0) == False',
                inplace=True)


def filter_origin_df(
        origin: int,
        df: Optional[pd.DataFrame] = None, 
        df_list: Optional[List[pd.DataFrame]] = None,
        origin_col: str = "origin_id"):
    """Filter origin_id column"""
    if df is not None:
        df_list = [df]
    if not df_list:
        raise ValueError("Please provide either df or df_list")
    for df in df_list:
        # input(df.head())
        df.query(
            f'{origin_col} == {origin}',
            inplace=True)


def load_plot(
        data: Path, grid: gp.GeoDataFrame, xbins: np.array, ybins: np.array,
        db_calc: tools.DbConn, bg: gp.GeoDataFrame,
        title: Optional[str] = "", origin: int = None,
        inverse: Optional[bool] = None,
        filter_null_island: bool = None,
        figsize: Tuple[int, int] = None,
        metric: str = "postcount_est", filename: str = None,
        chunk_size: int = CHUNK_SIZE, keep_hll: Optional[bool] = None,
        store_benchmark_data: Optional[bool] = None,
        data_list: List[Path] = None, plot: Optional[bool] = True,
        column_metric_ref: Dict[str, str] = COLUMN_METRIC_REF,
        reset_benchmark_data: Optional[bool] = None,
        output: Path = OUTPUT, 
        usecols: Tuple[str, str] = ['latitude', 'longitude'],
        proj_transformer: Transformer = None, 
        proj_transformer_back: Transformer = None,
        scheme: str = None, cmap_name: str = None,
        edgecolor: str = None
        ):
    """Load data, bin coordinates, estimate distinct counts (cardinality) and plot map
    
        Args:
        data: Path to read input CSV
        grid: A geopandas geodataframe with indexes x and y 
            (projected coordinates) and grid polys
        title: Title of the plot
        inverse: If True, inverse colors (black instead of white map)
        metric: target column for aggregate. Default: postcount_est.
        filename: Provide a name to store figure as PNG. Will append 
            '_inverse.png' if inverse=True.
        chunk_size: chunk processing into x records per chunk
        keep_hll: If True, hll sets will not be removed 
            after final estimation of cardinality
        store_benchmark_data: If True, hll data will be stored to separate
            output pickle file, ending with _hll.pkl
        data_list: Optionally provide a list of data paths that will be combined.
        plot: Plot & store figure (Default = True).
        reset_benchmark_data: Optionally remove/reset hll data from previous run.
    """
    benchmark_data = False
    if store_benchmark_data:
        benchmark_data = True
    if reset_benchmark_data is None:
        reset_benchmark_data = True
    if store_benchmark_data:
        keep_hll = True
    if inverse is None:
        inverse = False
    if filter_null_island is None:
        filter_null_island = True
    if proj_transformer is None:
        raise ValueError("proj_transformer must be defined")
    if reset_benchmark_data:
        hll_col = metric.replace("_est", "_hll")
        grid.drop(columns=[hll_col], inplace=True, errors='ignore')
    
    column = column_metric_ref.get(metric)
    usecols.append(column)
    # get data from csv
    chunked_df = read_project_chunked(
        filename=data,
        usecols=usecols,
        proj_transformer=proj_transformer)
    # optionally filter null island
    if filter_null_island:
        filter_nullisland_df(df_list=chunked_df)
    if origin:
        filter_origin_df(df_list=chunked_df, origin=origin)
    # bin coordinates
    bin_chunked_coordinates(chunked_df, xbins=xbins, ybins=ybins)
    # reset metric column
    reset_metrics(grid, [metric], setzero=False)
    print("Getting cardinality per bin..")
    # union hll sets per chunk and 
    # calculate distinct counts on last iteration
    join_chunkeddf_grid(
        chunked_df=chunked_df, grid=grid,
        metric=metric, chunk_size=chunk_size,
        keep_hll=keep_hll, db_calc=db_calc)
    # store intermediate data
    if benchmark_data:
        print("Storing aggregate data as pickle..")
        grid.to_pickle(OUTPUT / "pickles" / f"{filename}.pkl")
        print("Storing benchmark hll data as pickle..")
        grid[[metric.replace("_est", "_hll"), "geometry"]].to_pickle(
            OUTPUT / "pickles" / f"{filename}_hll.pkl")
        print("Storing benchmark hll data as csv..")
        if proj_transformer_back is None:
            warnings.warn(
                "proj_transformer_back must be defined for exporting benchmark data")
        tools.export_benchmark(
            grid=grid, filename=filename,
            metric=metric, output=OUTPUT,
            column_metric_ref=column_metric_ref,
            proj_transformer_back=proj_transformer_back)
    if plot:
        print("Plotting figure..")
        plot_figure(
            grid=grid, title=title, inverse=inverse,
            metric=metric, store_fig=filename,
            output=output, bg=bg, figsize=figsize, scheme=scheme,
            cmap_name=cmap_name, edgecolor=edgecolor)